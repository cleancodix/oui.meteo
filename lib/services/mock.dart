// ignore_for_file: non_constant_identifier_names
import 'dart:convert';

import 'package:http/http.dart';
import 'package:oui_sncf_weather/constants/config/environment.dart';
import 'package:oui_sncf_weather/constants/config/routes.dart';
import 'package:oui_sncf_weather/constants/mocks/users.dart';
import 'package:oui_sncf_weather/constants/mocks/weather.dart';
import 'package:oui_sncf_weather/models/user.data.dart';
import 'package:oui_sncf_weather/tools/digit.dart';
import 'package:oui_sncf_weather/tools/log.dart';
import 'package:oui_sncf_weather/tools/text.dart';

abstract class MockService {
  static const int MinMockDuration = 100; // in ms
  static const int MaxMockDuration = 1000; // in ms
  static const double Latitude = 50.8851467;
  static const double Longitude = 4.4186917;

  static Map<String, Map<String, dynamic> Function(String)> Paths = {
    '/login': onLogin,
    ...Map.fromEntries(Routes.Weather.keys
        .map((query) => MapEntry('/forecast?appid=${Environment.OpenWeatherApiKey}&units=metric&q=$query', onWeather))),
  };

  static Future<Response> call(Uri url, {Map<String, String> headers, body}) async {
    String path = url.toString().replaceAll(RegExp(Environment.HttpEndpoint), '');
    Function(Map<String, dynamic>) _response = (data) => Response(json.encode(data), 200);

    if (Paths.containsKey(path)) {
      if (path == '/login')
        await Future.delayed(Duration(seconds: 2));
      else
        await Future.delayed(Duration(milliseconds: DigitTool.getRandomNumber(MaxMockDuration, MinMockDuration)));
      return _response(Paths[path](body.toString()));
    }
    Log.print(path);
    throw Exception("Called path not mocked :/");
  }

  static String _getFromParams(String params, String search) {
    String found = TextTool.getStringBetweenTwo(params, '"$search":', ',');
    found ??= TextTool.getStringBetweenTwo(params, '"$search":', '}');
    found ??= TextTool.getStringBetweenTwo(params, '$search:', ',');
    found ??= TextTool.getStringBetweenTwo(params, '$search:', '}');

    if (found != null) {
      found = found.trim();
      if (found.startsWith('"') && found.endsWith('"')) {
        found = found.substring(1, found.length - 1);
        found = found.trim();
      }
    }

    return found;
  }

  static Map<String, dynamic> onLogin(String params) {
    String email = _getFromParams(params, "login");

    UserDataModel user = MockedUsers.firstWhere((user) => user.email == email, orElse: () => null);

    if (user == null) return {"error": "Invalid credentials"};

    return {
      "userData": {
        "id": user.id,
        "firstName": user.firstName,
        "lastName": user.lastName,
        "email": user.email,
        "password": user.password,
        "avatarUrl": user.avatarUrl,
        "sessionToken": user.sessionToken
      },
    };
  }

  static Map<String, dynamic> onWeather(String params) {
    String city = _getFromParams(params, "q");

    return MockedWeather.containsKey(city) ? MockedWeather[city] : null;
  }
}
