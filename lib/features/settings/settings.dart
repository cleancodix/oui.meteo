import 'package:mvcprovider/mvcprovider.dart';

import 'settings.ctrl.dart';
import 'settings.model.dart';
import 'settings.view.dart';

class SettingsPage extends MVC_Module<SettingsModel, SettingsView, SettingsCtrl> {
  final SettingsModel model = SettingsModel();
  final SettingsView view = SettingsView();
  final SettingsCtrl ctrl = SettingsCtrl();

  final List<SingleChildWidget> providers = [];
}
