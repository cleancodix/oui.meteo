import 'package:flutter/material.dart';
import 'package:mvcprovider/mvcprovider.dart';
import 'package:oui_sncf_weather/models/weather.data.dart';
import 'package:oui_sncf_weather/services/cache.dart';
import 'package:oui_sncf_weather/services/loading.dart';
import 'package:oui_sncf_weather/tools/log.dart';

import '../weather.provider.dart';
import 'weather.list.model.dart';

class WeatherListCtrl extends MVC_Controller<WeatherListModel> {
  CacheService get cacheService => CacheService(context).get;
  LoadingService get loading => LoadingService(context).get;
  GlobalKey<RefreshIndicatorState> refreshKey = GlobalKey<RefreshIndicatorState>();

  // Called one time after first view rendering
  @override
  void init() {
    if (cacheService.isWeatherListExpired(model.city))
      Future.delayed(Duration(milliseconds: 50), () => requestWeathers());
    // Yes it's ugly but there is a regression since 2 years that I don't opened this project :/
  }

  Future<void> requestWeathers([bool force = false]) async {
    loading.start(model.loadingId);
    refreshKey.currentState.show();
    try {
      Log.print('Retrieving ${model.city} images list', title: 'WeatherListCtrl');
      List<WeatherDataModel> weathers = await WeatherProvider(context).getWeatherList(model.city, force);
      if (weathers != null) {
        model.weathers = weathers;
        cacheService.setWeathers(model.city, weathers);
        notifyListeners();
      }
      loading.stop(model.loadingId);
    } catch (e, stackTrace) {
      Log.print('Error while retrieving "${model.city}" weathers',
          title: 'WeatherListCtrl', error: e, stackTrace: stackTrace);
    }
    loading.stop(model.loadingId);
  }
}
