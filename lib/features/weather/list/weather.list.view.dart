
import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/components/labels/subtitle.label.dart';
import 'package:oui_sncf_weather/components/layout/custom.sliver.bar.dart';
import 'package:oui_sncf_weather/components/layout/scroll.page.dart';
import 'package:oui_sncf_weather/constants/theme/theme.sizes.dart';
import 'package:oui_sncf_weather/services/loading.dart';
import 'package:oui_sncf_weather/services/translation.dart';
import 'package:oui_sncf_weather/tools/text.dart';
import 'package:mvcprovider/mvcprovider.dart';

import 'components/weather.list.item.dart';
import 'weather.list.ctrl.dart';
import 'weather.list.model.dart';

class WeatherListView extends StatelessWidget with MVC_View<WeatherListModel, WeatherListCtrl> {

  @override
  Widget build(BuildContext context) {
    listen(context);

    Widget mainContent = model.weathers != null && model.weathers.length > 0 ? SliverList(
      delegate: SliverChildBuilderDelegate(
              (context, index) => WeatherListItem(model.weathers[index], model.city),
          childCount: model.weathers.length
      ),
    ) : SliverFillRemaining(
      child: !LoadingService(context).listen.loading(model.loadingId) ? Center(
        child: Padding(
          padding: ThemeSizes.padding[Sizes.L],
          child: SubtitleLabel(
            TranslationService(context).get["No weather found with this city"],
            maxLines: 3,
          ),
        ),
      ) : Container(),
    );

    return ScrollPage(
      refreshKey: ctrl.refreshKey,
      overlap: !model.showAppBar,
      slivers: <Widget>[
        if (model.showAppBar) CustomSliverBar(
          title: SubtitleLabel(TextTool.capitalize(model.city), heroTag: "TITLE-${model.city}"),
        ),
        mainContent,
      ],
      onRefresh: () => ctrl.requestWeathers(true),
    );
  }
}