import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/tools/log.dart';

class SliverView extends StatelessWidget {

  final Widget appBar;
  final Widget child;

  SliverView({this.appBar, this.child});

  static Widget overlap(BuildContext context) {
    Widget result;
    try {
      result = SliverOverlapInjector(handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context));
    } catch(e) {
      Log.print("NestedScrollView's context not found", title: "SliverView's overlap fct");
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return [
            SliverOverlapAbsorber(
                handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                sliver: appBar
            ),
          ];
        },
        body: child
    );
  }
}
