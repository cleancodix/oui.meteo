
import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/constants/config/routes.dart';
import 'package:oui_sncf_weather/services/route.dart';

class RouteDataModel {
  final String name;
  final Widget Function(BuildContext) widget;
  final List<Guard> guards;
  final List<Guard> onPop;
  final List<RouteParams> params;
  final List<String> history;

  RouteDataModel({this.name, this.widget, this.params, this.guards, this.onPop, this.history}) {
    Routes.Pages[name] = widget;
    if (this.guards != null) Routes.Guards[name] = guards;
    if (this.onPop != null) Routes.PopGuards[name] = onPop;
    if (this.params != null) Routes.Params[name] = params;
    if (this.history != null) Routes.Histories[name] = history;
  }
}