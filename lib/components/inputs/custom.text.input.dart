import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/components/labels/error.label.dart';
import 'package:oui_sncf_weather/components/misc/shake.on.trigger.dart';
import 'package:oui_sncf_weather/constants/config/test.keys.dart';
import 'package:oui_sncf_weather/constants/theme/theme.sizes.dart';
import 'package:oui_sncf_weather/services/translation.dart';
import 'package:oui_sncf_weather/tools/widget.dart';

class CustomTextInput extends StatefulWidget {
  CustomTextInput({this.testKey, this.model, this.suffix, this.textAlign}) : super(key: model.key);

  final CustomTextInputModel model;
  final String testKey;
  final Widget suffix;
  final TextAlign textAlign;

  @override
  State<StatefulWidget> createState() => CustomTextInputState();
}

class CustomTextInputState extends State<CustomTextInput> with SingleTickerProviderStateMixin {
  GlobalKey<ShakeOnTriggerState> shakeKey = GlobalKey<ShakeOnTriggerState>();
  Icon get icon => widget.model.icon != null
      ? Icon(widget.model.icon.icon, size: widget.model.icon.size, color: widget.model.iconColor)
      : null;
  void rebuild() => setState(() {});

  @override
  Widget build(BuildContext context) {
    widget.model.context = context;
    widget.model.controller.selection =
        TextSelection.fromPosition(TextPosition(offset: widget.model.controller.text.length));
    return ShakeOnTrigger(
        key: shakeKey,
        child: Stack(
          alignment: Alignment.bottomRight,
          children: [
            Padding(
              padding: widget.model.padding,
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight: widget.model.inputHeight,
                  maxHeight: widget.model.inputHeight,
                ),
                child: Stack(
                  children: [
                    WidgetTool.wrapWithHero(
                        widget.model.backgroundHeroTag,
                        Material(
                          color: Theme.of(context).inputDecorationTheme.fillColor,
                          elevation: widget.model.shadow,
                          borderRadius: widget.model.borderRadius,
                          shadowColor: Theme.of(context).shadowColor,
                          child: Container(),
                        )),
                    Container(
                      child: TextFormField(
                        key: Key(widget.testKey),
                        autocorrect: false,
                        enableSuggestions: false,
                        validator: widget.model.validateOnChange,
                        keyboardType: widget.model.keyboard,
                        focusNode: widget.model.focusNode,
                        controller: widget.model.controller,
                        obscureText: widget.model.secret,
                        textAlign: widget.textAlign ?? widget.model.textAlign,
                        textAlignVertical: TextAlignVertical.center,
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(horizontal: widget.model?.icon?.size ?? 0),
                            prefixIcon: WidgetTool.wrapWithHero(widget.model.iconHeroTag, icon),
                            suffixIcon: widget.suffix != null ? widget.suffix : null,
                            prefixIconConstraints: BoxConstraints(
                                minWidth: ThemeSizes.inputHeight[Sizes.M], minHeight: ThemeSizes.inputHeight[Sizes.M]),
                            labelText: widget.model.label,
                            labelStyle: Theme.of(context).textTheme.caption,
                            border: InputBorder.none),
                        onChanged: (String str) {
                          widget.model.value = str;
                          widget.model.onChanged(str);
                        },
                        onFieldSubmitted: (String str) {
                          widget.model.validateOnChange(str);
                          widget.model.focusNode.unfocus();
                          if (widget.model.next is FocusNode) FocusScope.of(context).requestFocus(widget.model.next);
                          if (widget.model.next is Function) widget.model.next();
                        },
                        onSaved: (String str) {
                          widget.model.validateOnSubmit(str);
                          if (!widget.model.isValid) shakeKey.currentState.animate();
                        },
                        style: Theme.of(context).textTheme.caption,
                        maxLength: widget.model.showMaxLength ? widget.model.maxLength : null,
                        maxLines: widget.model.maxLines ?? 1,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            ...(widget.model.currentMessage != null
                ? [
                    ErrorLabel(
                      widget.model.currentMessage,
                      testKey: "${widget.testKey}.${Keys.ErrorLabel.toString()}",
                    )
                  ]
                : [])
          ],
        ));
  }
}

class CustomTextInputModel {
  CustomTextInputModel({String value, this.next, String label, String invalidMessage, String errorMessage})
      : this._label = label,
        this._invalidMessage = invalidMessage,
        this._errorMessage = errorMessage {
    controller.text = value;
    this.value = value;
  }

  GlobalKey<CustomTextInputState> key = GlobalKey<CustomTextInputState>();
  void rebuild() => key?.currentState?.rebuild();

  String _value;
  String get value => _value;
  set value(String str) {
    _value = str;
    isEmpty = str == null || str.length == 0;
    if (isEmpty)
      controller.clear();
    else
      controller.text = str;
    isValid = (isSubmit ? validate() : validate() || isEmpty) && !isError;
  }

  bool secret = false;
  bool required = false;

  int minLength;
  int maxLength;
  int maxLines;
  bool showMaxLength = false;

  String currentMessage;
  TextInputType keyboard;
  dynamic next;

  Map<String, String> get translate => TranslationService(context).get;

  final String _label;
  String get label => _label;
  final String _invalidMessage;
  String get invalidMessage => _invalidMessage;
  final String _errorMessage;
  String get errorMessage => _errorMessage;

  bool isValid;
  bool isEmpty;
  bool isError = false;
  bool isSubmit = false;

  Icon icon;
  TextAlign textAlign = TextAlign.left;
  double shadow = ThemeSizes.shadow[Sizes.S];
  BorderRadius borderRadius = ThemeSizes.borderRadius[Sizes.S];
  EdgeInsets padding = ThemeSizes.paddingOnly[AxisDirection.down][Sizes.M];
  double inputHeight = ThemeSizes.inputHeight[Sizes.M];

  String iconHeroTag;
  String backgroundHeroTag;

  final FocusNode focusNode = FocusNode();
  final TextEditingController controller = TextEditingController();
  BuildContext context;

  double get height => padding.vertical + inputHeight;

  Color get iconColor => Theme.of(context).iconTheme.color;

  bool validate() {
    return (!required || required && !isEmpty) &&
        (minLength == null || minLength != null && value.length >= minLength) &&
        (maxLength == null || maxLength != null && value.length <= maxLength);
  }

  String validateOnChange([String str]) {
    value = str ?? value;
    getErrorMessage();
    return null;
  }

  String validateOnSubmit([String str]) {
    isSubmit = true;
    value = str ?? value;
    isSubmit = false;
    return getErrorMessage();
  }

  String getErrorMessage() {
    String msg = isValid == null || isValid ? null : invalidMessage;
    currentMessage = isError ? errorMessage : msg;
    isError = false;
    return currentMessage;
  }

  void onChanged(String str) {}
}
