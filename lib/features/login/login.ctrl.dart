import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:mvcprovider/mvcprovider.dart';
import 'package:oui_sncf_weather/components/inputs/custom.form.dart';
import 'package:oui_sncf_weather/components/inputs/custom.text.input.dart';
import 'package:oui_sncf_weather/components/inputs/email.text.input.dart';
import 'package:oui_sncf_weather/constants/config/routes.dart';
import 'package:oui_sncf_weather/models/user.data.dart';
import 'package:oui_sncf_weather/services/cache.dart';
import 'package:oui_sncf_weather/services/layout.dart';
import 'package:oui_sncf_weather/services/loading.dart';
import 'package:oui_sncf_weather/services/route.dart';
import 'package:oui_sncf_weather/services/translation.dart';
import 'package:oui_sncf_weather/tools/log.dart';

import 'login.model.dart';
import 'login.provider.dart';

class LoginCtrl extends MVC_Controller<LoginModel> {
  final GlobalKey<CustomFormState> formKey = GlobalKey<CustomFormState>();
  CacheService get cacheService => CacheService(context).get;
  LayoutService get layoutService => LayoutService(context).get;
  LoadingService get loadingService => LoadingService(context).get;

  // Called on submit button click or after validating password input with keyboard
  void onSubmit() {
    model.errorMsg = null;
    loadingService.start(model.loadingId);
    LoginProvider(context)
        .login(model.email.value, model.password.value)
        .then((user) => onLoginResponse(user), onError: onLoginError);
  }

  // Called when LoginService.login return something
  Future<void> onLoginResponse(UserDataModel user) async {
    try {
      if (user == null)
        await onLoginFail(true);
      else
        await onLoginSuccess(user);
    } catch (e, stackTrace) {
      Log.print("onLoginError", title: 'LoginCtrl', error: e, stackTrace: stackTrace);
      onLoginError(e, false);
    }
  }

  // Called when LoginService.login throw an error
  void onLoginError(Object error, [bool showToast = true]) {
    loadingService.stop(model.loadingId);
    FocusScope.of(context).unfocus();
    model.errorMsg = TranslationService(context).get["Error, check your connection\nor try again later."];
  }

  // Called when LoginService.login returned null
  Future<void> onLoginFail([bool isSubmit = false]) async {
    model.errorMsg = null;
    Log.print('onLoginFail', title: 'LoginCtrl');
    model.password.isError = isSubmit;
    formKey.currentState.validateForSubmit();
    await model.removeSavedCredential();
    loadingService.stop(model.loadingId);
  }

  // Called when LoginService.login returned a user
  Future<void> onLoginSuccess(UserDataModel user) async {
    await model.saveCredential();
    cacheService.addUsers([user]);
    cacheService.loggedUserId = user.id;
    loadingService.stop(model.loadingId);
    Log.print(
      "id : ${user.id}\n"
      "firstName : ${user.firstName}\n"
      "lastName : ${user.lastName}\n"
      "email : ${user.email}",
      title: "Login success ${cacheService.isLogged}",
    );
    RouteService(context).get.goTo(Routes.Home, removeUntil: true);
  }

  void showForgotPasswordDialog() {
    FocusScope.of(context).unfocus();

    if (model.forgotPwdEmail == null)
      model.forgotPwdEmail = EmailTextInputModel();
    else {
      notifyListeners();
    }

    layoutService.alert(
        barrierDismissible: true,
        title: TranslationService(context).get["Reset my password"],
        body: Column(
          children: [
            AutoSizeText(TranslationService(context).get["Confirm your email :"], maxLines: 1),
            CustomTextInput(model: model.forgotPwdEmail)
          ],
        ),
        buttons: {
          TranslationService(context).get["Cancel"]: Navigator.of(context).pop,
          TranslationService(context).get["Validate"]: () => resetPassword(context)
        });
  }

  resetPassword(BuildContext context) async {
    model.forgotPwdEmail.validateOnChange();
    Log.print("Confirm email ${model.forgotPwdEmail.value}", title: "Reset Password");

    try {
      int response = await LoginProvider(context).forgotPassword(model.forgotPwdEmail.value);
      Log.print("ForgotPassword response : $response", title: "Reset Password");

      if (response == 1) {
        layoutService.showToast(TranslationService(context).get["A new password has been sent to you by email."]);
      } else {
        Log.print("Error while reseting password", title: "Reset Password");
        layoutService.showToast(
            "${TranslationService(context).get["The password associated with the email"]} ${model.forgotPwdEmail.value} ${TranslationService(context).get["could not be reset."]}");
      }
      Navigator.of(context).pop();
    } catch (e) {
      Log.print("Error while reseting password", error: e);
      layoutService.showToast(
          "${TranslationService(context).get["The password associated with the email"]} ${model.forgotPwdEmail.value} ${TranslationService(context).get["could not be reset."]}");
      Navigator.of(context).pop();
    }
  }
}
