import 'package:mvcprovider/mvcprovider.dart';
import 'package:oui_sncf_weather/services/route.dart';

import 'weather.list.ctrl.dart';
import 'weather.list.model.dart';
import 'weather.list.view.dart';

class WeatherListModule extends MVC_Module<WeatherListModel, WeatherListView, WeatherListCtrl> {
  static WeatherListModel _model;
  final WeatherListModel model;
  final WeatherListView view = WeatherListView();
  final WeatherListCtrl ctrl = WeatherListCtrl();

  final List<SingleChildWidget> providers = [];

  WeatherListModule(String city) : model = WeatherListModel(city) {
    _model = model;
  }

  static final List<Guard> onPop = [
    (_context, destination) async {
      _model.initAppBar(true);
      return true;
    },
  ];
}
