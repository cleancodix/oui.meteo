
const Map<String, String> ENGLISH = {


  //--------------------- LOGIN ------------------------

  "Log in": "Log in",
  "Error, check your connection\nor try again later.": "Error, check your connection\nor try again later.",

  "Remember me": "Remember me",
  "Forgot your password ?": "Forgot your password ?",

  "Reset my password": "Reset my password",
  "Confirm your email :": "Confirm your email :",

  "Cancel": "Cancel",
  "Validate": "Validate",

  "A new password has been sent to you by email.": "A new password has been sent to you by email.",
  "The password associated with the email": "The password associated with the email",
  "could not be reset.": "could not be reset.",



  //--------------------- HOME ------------------------

  "Welcome": "Welcome",
  "No city selected, please add some cities by going in Menu/Settings": "No city selected, please add some cities by going in Menu/Settings",



  //--------------------- WEATHER ------------------------

  "No weather found with this city": "No weather found with this city",
  "thunderstorm with light rain": "thunderstorm with light rain",
  "thunderstorm with rain": "thunderstorm with rain",
  "thunderstorm with heavy rain": "thunderstorm with heavy rain",
  "light thunderstorm": "light thunderstorm",
  "thunderstorm": "thunderstorm",
  "heavy thunderstorm": "heavy thunderstorm",
  "ragged thunderstorm": "ragged thunderstorm",
  "thunderstorm with light drizzle": "thunderstorm with light drizzle",
  "thunderstorm with drizzle": "thunderstorm with drizzle",
  "thunderstorm with heavy drizzle": "thunderstorm with heavy drizzle",
  "light intensity drizzle":
  "light intensity drizzle",
  "drizzle":
  "drizzle",
  "heavy intensity drizzle":
  "heavy intensity drizzle",
  "light intensity drizzle rain":
  "light intensity drizzle rain",
  "rain": "rain",
  "drizzle rain":
  "drizzle rain",
  "heavy intensity drizzle rain":
  "heavy intensity drizzle rain",
  "shower rain and drizzle":
  "shower rain and drizzle",
  "heavy shower rain and drizzle":
  "heavy shower rain and drizzle",
  "shower drizzle":
  "shower drizzle",
  "light rain": "light rain",
  "moderate rain": "moderate rain",
  "heavy intensity rain": "heavy intensity rain",
  "very heavy rain": "very heavy rain",
  "extreme rain": "extreme rain",
  "freezing rain": "freezing rain",
  "light intensity shower rain": "light intensity shower rain",
  "shower rain": "shower rain",
  "heavy intensity shower rain": "heavy intensity shower rain",
  "ragged shower rain": "ragged shower rain",
  "light snow": "light snow",
  "Snow": "Snow",
  "snow": "snow",
  "Heavy snow": "Heavy snow",
  "Sleet": "Sleet",
  "Light shower sleet": "Light shower sleet",
  "Shower sleet": "Shower sleet",
  "Light rain and snow": "Light rain and snow",
  "Rain and snow": "Rain and snow",
  "Light shower snow": "Light shower snow",
  "Shower snow": "Shower snow",
  "Heavy shower snow": "Heavy shower snow",
  "mist": "mist",
  "Smoke": "Smoke",
  "Haze": "Haze",
  "sand/ dust whirls": "sand/ dust whirls",
  "fog": "fog",
  "sand": "sand",
  "dust": "dust",
  "volcanic ash": "volcanic ash",
  "squalls": "squalls",
  "tornado": "tornado",
  "clear sky": "clear sky",
  "few clouds: 11-25%": "few clouds: 11-25%",
  "scattered clouds: 25-50%": "scattered clouds: 25-50%",
  "broken clouds: 51-84%": "broken clouds: 51-84%",
  "overcast clouds: 85-100%": "overcast clouds: 85-100%",
  "few clouds": "few clouds",
  "scattered clouds": "scattered clouds",
  "broken clouds": "broken clouds",
  "overcast clouds": "overcast clouds",



  //--------------------- SETTINGS ------------------------

  "Settings": "Settings",
  "Add a city here": "Add a city here",
  "Theme": "Theme",
  "Light": "Light",
  "Dark": "Dark",



  //--------------------- COMPONENTS ------------------------


  // SIDE MENU (DRAWER)

  "Log out": "Log out",


  // EMAIL TEXT INPUT

  "E-mail": "E-mail",
  "Username": "Username",
  "Invalid e-mail": "Invalid e-mail",


  // PASSWORD TEXT INPUT

  "Password": "Password",
  "characters minimum": "characters minimum",
  "Incorrect credentials": "Incorrect credentials",



  //--------------------- TOOLS ------------------------

  // DATE

  "Tomorrow": "Tomorrow",
  "Today": "Today",
  "Yesterday": "Yesterday",
  "at": "at",
  "day1": "Monday",
  "day2": "Tuesday",
  "day3": "Wednesday",
  "day4": "Thursday",
  "day5": "Friday",
  "day6": "Saturday",
  "day7": "Sunday",


  //--------------------- SERVICES ------------------------

  "Thank you to grant location": "Thank you to grant location permission to our application in your device settings",
  "Thank you to enable location service": "Thank you to enable location service"


};
