
const Map<dynamic, String> Assets = {
  Logos.Vertical: "assets/oui_sncf_weather/logos/logo-vertical.png",
  Logos.Horizontal: "assets/oui_sncf_weather/logos/logo-horizontal.png",
  Logos.VerticalDark: "assets/oui_sncf_weather/logos/logo-vertical-dark.png",
  Logos.HorizontalDark: "assets/oui_sncf_weather/logos/logo-horizontal-dark.png",
  Logos.EasterEgg: "assets/oui_sncf_weather/logos/easter-egg.png",
};

enum Backgrounds {
  White,
}

enum Logos {
  Vertical,
  Horizontal,
  VerticalDark,
  HorizontalDark,
  EasterEgg
}