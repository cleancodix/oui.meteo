import 'dart:async';

import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/constants/config/routes.dart';
import 'package:oui_sncf_weather/constants/theme/theme.colors.dart';
import 'package:oui_sncf_weather/services/cache.dart';
import 'package:oui_sncf_weather/services/route.dart';
import 'package:oui_sncf_weather/services/theme.dart';
import 'package:oui_sncf_weather/tools/log.dart';

import 'loading.dart';

class GuardService {
  // ignore: unused_element
  GuardService._();

  static List<String> pagesWithoutAuthentication = [Routes.Login];
  static List<String> pagesWithWhiteBackground = [Routes.Login];
  static List<String> pagesWithDefaultTheme = [Routes.Login];

  // First guards that will be executed for each routes that have guards
  static List<Guard> goToStartingGuards = [
    startLoading,
    authentication,
  ];

  // Last guards that will be executed for each routes that have guards
  static List<Guard> goToEndingGuards = [checkParams, resetColorTheme, stopLoading];

  // Guards that will be executed for each pop events
  static List<Guard> poppingGuards = [
    resetColorTheme,
  ];

  static Future<bool> authentication(BuildContext context, Destination destination) async {
    if (pagesWithoutAuthentication.contains(destination.next)) return true;
    bool result = CacheService(context).get.loggedUser != null;
    if (!result)
      Log.print("No logged user found in CacheService",
          title: "GuardService", error: "Authentication guard not passed");
    return result;
  }

  static Future<bool> checkParams(BuildContext context, Destination destination) async {
    bool result = true;
    String msg = "Route ${destination.next}'s params miss: ";
    (Routes.Params[destination.next] ?? []).forEach((params) {
      if (!destination.params.keys.contains(params)) {
        result = false;
        msg += "\n${params.toString()}";
      }
    });
    if (!result) Log.print(msg, title: "GuardService", error: "CheckParams guard not passed");
    return result;
  }

  static Future<bool> startLoading(BuildContext context, Destination destination) async {
    return LoadingService(context).get.start(destination.next);
  }

  static Future<bool> stopLoading(BuildContext context, Destination destination) async {
    return LoadingService(context).get.stop(destination.next);
  }

  static Future<bool> resetColorTheme(BuildContext context, Destination destination) async {
    if (pagesWithDefaultTheme.contains(destination.next)) {
      ThemeService(context).get.selectTheme(ThemeColors.Switcher.keys.first);
    }
    return true;
  }
}
