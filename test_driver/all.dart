
import 'home_test.dart';
import 'login_test.dart';

// Run all tests with : flutter drive --target=test_driver/main.dart --driver=test_driver/all.dart --dart-define=TESTS=true

void main() {
  LoginTest.run();
  HomeTest.run();
}