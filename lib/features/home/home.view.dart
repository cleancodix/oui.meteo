
import 'package:flutter/material.dart';
import 'package:oui_sncf_weather/components/labels/subtitle.label.dart';
import 'package:oui_sncf_weather/components/layout/custom.sliver.bar.dart';
import 'package:oui_sncf_weather/components/layout/sliver.view.dart';
import 'package:oui_sncf_weather/constants/config/routes.dart';
import 'package:oui_sncf_weather/constants/theme/theme.sizes.dart';
import 'package:oui_sncf_weather/services/translation.dart';
import 'package:mvcprovider/mvcprovider.dart';

import '../../components/images/main.logo.dart';
import 'home.ctrl.dart';
import 'home.model.dart';

class HomeView extends StatefulWidget  {

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> with MVC_View<HomeModel, HomeCtrl>, TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    listen(context);
    Map<String, String> translate = TranslationService(context).get;
    MainLogo mainLogo = MainLogo(axis: Axis.horizontal);

    TabController tabController = TabController(
        initialIndex: model.currentTabIndex,
        length: Routes.Weather.keys.length,
        vsync: this
    );

    Widget mainContent = model.tabViews.length > 0 ? TabBarView(
        controller: tabController,
        children: model.tabViews
    ): Center(
      child: Padding(
        padding: ThemeSizes.padding[Sizes.L],
        child: SubtitleLabel(
          translate["No city selected, please add some cities by going in Menu/Settings"],
          maxLines: 3,
        ),
      ),
    );

    return SliverView(
      appBar: CustomSliverBar(
            background: Stack(
              children: [
                Center(child: mainLogo),
                SafeArea(
                    child: Padding(
                      padding: ThemeSizes.paddingSymmetric[Sizes.S][Sizes.M],
                      child: SubtitleLabel("${translate["Welcome"]} ${model.cache.loggedUser.firstName}"),
                    )
                )
              ],
            ),
            expandedHeight: mainLogo.height,
            collapsed: TabBar(
              controller: tabController,
              isScrollable: true,
              onTap: ctrl.onTabTap,
              tabs: Routes.Weather.keys.map((key) => Hero(
                tag: "TITLE-$key",
                child: Tab(text: key),
              )).toList(),
              labelColor: Theme.of(context).colorScheme.onPrimary,
            ),
            padding: ThemeSizes.padding[Sizes.Zero],
        ),
        child: mainContent
    );
  }
}