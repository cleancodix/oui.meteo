// ignore_for_file: non_constant_identifier_names
import 'package:oui_sncf_weather/constants/config/storage.keys.dart';
import 'package:oui_sncf_weather/constants/theme/theme.colors.dart';
import 'package:oui_sncf_weather/constants/theme/theme.sizes.dart';
import 'package:oui_sncf_weather/services/local.storage.dart';
import 'package:oui_sncf_weather/tools/log.dart';
import 'package:flutter/material.dart';
import 'package:mvcprovider/mvcprovider.dart';
import 'package:provider/provider.dart';

class ThemeService extends MVC_Notifier<ThemeService> {

  ThemeService([context]) : super(context);

  @override
  ChangeNotifierProvider<ThemeService> get create {
    _initMode();
    return super.create;
  }

  String currentTheme = ThemeColors.Switcher.keys.first;
  ThemeMode mode = ThemeMode.system;
  bool get isDark => Theme.of(context).brightness == Brightness.dark;

  Future<void> _initMode() async {
    String _mode = await LocalStorageService.get(StorageKeys.ThemeMode);
    if (_mode != null) mode = _mode == "ThemeMode.dark" ? ThemeMode.dark : ThemeMode.light;
    String _theme = await LocalStorageService.get(StorageKeys.SelectedTheme);
    if (_theme != null) {
      currentTheme = _theme;
      ThemeColors.Switcher[_theme]();
    }
    notifyListeners();
  }

  Future<void> selectTheme(String theme) async {
    if (currentTheme == theme) return;
    currentTheme = theme;
    ThemeColors.Switcher[theme]();
    Log.print('New theme selected: $theme', title: "ThemeService");
    await LocalStorageService.store(StorageKeys.SelectedTheme, theme);
    notifyListeners();
  }

  Future<void> toggleMode() async {
    mode = !isDark ? ThemeMode.dark : ThemeMode.light;
    Log.print('Theme mode changed: ${mode.toString()}');
    await LocalStorageService.store(StorageKeys.ThemeMode, mode.toString());
    notifyListeners();
  }

  IconData get icon {
    switch (mode) {
      case ThemeMode.light: return Icons.wb_sunny;
      case ThemeMode.dark: return Icons.nights_stay;
      default: return Icons.system_update;
    }
  }

  static Map<Sizes, T> getSizes<T>(double value, [T Function(double value) parser]) {

    Map<Sizes, T> result = {};

    SizeRatios.forEach((size, ratio) {
      result[size] = parser != null ? parser(value * ratio) : value * ratio;
    });

    return result;
  }
}