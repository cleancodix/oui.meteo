import 'package:mvcprovider/mvcprovider.dart';
import 'package:oui_sncf_weather/components/inputs/custom.text.input.dart';
import 'package:oui_sncf_weather/constants/config/routes.dart';
import 'package:oui_sncf_weather/constants/config/test.keys.dart';
import 'package:oui_sncf_weather/constants/theme/theme.colors.dart';
import 'package:oui_sncf_weather/features/settings/components/city.text.input.dart';
import 'package:oui_sncf_weather/features/settings/settings.ctrl.dart';
import 'package:oui_sncf_weather/services/cache.dart';
import 'package:oui_sncf_weather/services/translation.dart';

class SettingsModel extends MVC_Model<SettingsCtrl> {
  String errorMsg;
  String get title => translate["Settings"];
  Map<String, String> get translate => TranslationService(context).get;
  CacheService get cache => CacheService(context).get;

  List<String> cities;
  CustomTextInputModel tagInput;
  Features loadingId = Features.Settings;
  Map<String, String> themes;

  @override
  void init() {
    cities = Routes.Weather.values.toList();
    tagInput = CityTextInputModel(next: () => ctrl.formKey.currentState.submit());
    themes = Map.fromEntries(ThemeColors.Switcher.keys.map((key) => MapEntry(key, key)));
  }
}
